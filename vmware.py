#!/usr/bin/python
import collections,time,sys,re,requests
try:
    from pyVim import connect
    from pyVmomi import vim
    import pyVmomi
except:
    print "Install pyvmomi and its requirements first to make this work."
    sys.exit(2)

# All of the functions in here SHOULD work just fine. If not, get with Z.
# Most were either taken from or modified from the pyvmomi sample page.
# Also ensure you import all of the pyvmomi modules before importing this.

# Used in conjunction with print to put color in your text.
def printColored(textToPrint, color):
    if color == 'red':
        return "\033[31m" + textToPrint + "\033[0m"
    elif color == 'yellow':
        return "\033[33m" + textToPrint + "\033[0m"
    elif color == 'blue':
        return "\033[34m" + textToPrint + "\033[0m"
    elif color == 'purple':
        return "\033[35m" + textToPrint + "\033[0m"
    elif color == 'aqua':
        return "\033[36m" + textToPrint + "\033[0m"
    elif color == 'white':
        return "\033[37m" + textToPrint + "\033[0m"
    else:
        return textToPrint;

# This will remove any unicode markings within a dictionary.
def convert(data):
    if isinstance(data, basestring):
        return str(data)
    elif isinstance(data, collections.Mapping):
        return dict(map(convert, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(convert, data))
    else:
        return data

# This function and get_container_view are used to get all of the VMs known by vCenter. Its MUCH
# than the default way posted on pyvmomi. IE: This doesn't take 10+ minutes to get all VMs.
def collect_properties(service_instance, view_ref, obj_type, path_set=None, include_mors=False):
    """
    Collect properties for managed objects from a view ref
    Check the vSphere API documentation for example on retrieving
    object properties:
        - http://goo.gl/erbFDz
    Args:
        si          (ServiceInstance): ServiceInstance connection
        view_ref (pyVmomi.vim.view.*): Starting point of inventory navigation
        obj_type      (pyVmomi.vim.*): Type of managed object
        path_set               (list): List of properties to retrieve
        include_mors           (bool): If True include the managed objects
                                       refs in the result
    Returns:
        A list of properties for the managed objects
    """
    collector = service_instance.content.propertyCollector
    # Create object specification to define the starting point of
    # inventory navigation
    obj_spec = pyVmomi.vmodl.query.PropertyCollector.ObjectSpec()
    obj_spec.obj = view_ref
    obj_spec.skip = True
    # Create a traversal specification to identify the path for collection
    traversal_spec = pyVmomi.vmodl.query.PropertyCollector.TraversalSpec()
    traversal_spec.name = 'traverseEntities'
    traversal_spec.path = 'view'
    traversal_spec.skip = False
    traversal_spec.type = view_ref.__class__
    obj_spec.selectSet = [traversal_spec]
    # Identify the properties to the retrieved
    property_spec = pyVmomi.vmodl.query.PropertyCollector.PropertySpec()
    property_spec.type = obj_type
    if not path_set:
        property_spec.all = True
    property_spec.pathSet = path_set
    # Add the object and property specification to the
    # property filter specification
    filter_spec = pyVmomi.vmodl.query.PropertyCollector.FilterSpec()
    filter_spec.objectSet = [obj_spec]
    filter_spec.propSet = [property_spec]
    # Retrieve properties
    props = collector.RetrieveContents([filter_spec])
    data = []
    for obj in props:
        properties = {}
        for prop in obj.propSet:
            properties[prop.name] = prop.val
        if include_mors:
            properties['obj'] = obj.obj
        data.append(properties)
    return data

# This function and collect_properties are used to get all of the VMs known by vCenter. Its MUCH
# than the default way posted on pyvmomi. IE: This doesn't take 10+ minutes to get all VMs.
def get_container_view(service_instance, obj_type, container=None):
    """
    Get a vSphere Container View reference to all objects of type 'obj_type'
    It is up to the caller to take care of destroying the View when no longer
    needed.
    Args:
        obj_type (list): A list of managed object types
    Returns:
        A container view ref to the discovered managed objects
    """
    if not container:
        container = service_instance.content.rootFolder
    view_ref = service_instance.content.viewManager.CreateContainerView(container=container, type=obj_type, recursive=True)
    return view_ref

# This is a simple function to return the UUID of an object based on its type.
def get_obj(content, vimtype, name):
    """
    Return an object by name, if name is None the
    first found object is returned
    Valid vimtypes include: [vim.Folder] [vim.VirtualMachine]...
    """
    obj = None
    container = content.viewManager.CreateContainerView(content.rootFolder, vimtype, True)
    for c in container.view:
        if c.name == name:
                obj = c
                break
    return obj

# This is not the fastest, but it does work. It will find the name of the datastore with the most free space that matches
# the regex search string.
def findDatastore(service_instance,regex=None):
    container = service_instance.content.rootFolder
    # First we need to find all of the datastores
    datastores = service_instance.content.viewManager.CreateContainerView(container=container, type=[vim.Datastore], recursive=True)
    # Create a blank dictionary for use in a second
    databaseDict = {}
    for d in datastores.view:
        # Append to the dictionary so that we can sort by free space later
        databaseDict.update({d.name: [d.summary.freeSpace,d]})
    # If the user specified a regex to search for, use that, otherwise use the default one
    if regex:
        good_datastore = re.compile(regex)
    else:
        good_datastore = re.compile("^(DATASTORE_[0-9][0-9])$") # Use a naming scheme DATASTORE_XX where XX is a number

    # Now that we have a dictionary with all of the datastores and how much free space they have, lets sort by free space
    for datastoreName, datastoreSummary in sorted(databaseDict.items(), key=lambda databaseDict: databaseDict[1], reverse=True):
        # If the name of the datastore matches the standard for VMs, we are going to break out and use that datastore
        if good_datastore.match(datastoreName):
            #print "%s : %s" % (datastoreName, datastoreFree)
            return datastoreSummary[1]
            break

# Waits and provides updates on a vSphere task
def WaitTask(task, actionName='job', hideResult=False):
    while task.info.state == vim.TaskInfo.State.running:
        try:
            progress = task.info.progress
            sys.stdout.write("\rProgress: %i%% " % progress)
            sys.stdout.flush()
        except:
            pass # Don't display any progress
        time.sleep(2)
    if task.info.state == vim.TaskInfo.State.success:
        sys.stdout.write("\rProgress: 100%\n")
        sys.stdout.flush()
        if task.info.result is not None and not hideResult:
            out = '%s completed successfully, result: %s' % (actionName, task.info.result)
        else:
            out = '%s completed successfully.' % actionName
    else:
        sys.stdout.flush()
        out = '%s did not complete successfully: %s' % (actionName, task.info.error)
        print out
        #raise task.info.error # should be a Fault... check XXX
    # may not always be applicable, but can't hurt.
    return task.info.result

# Function to change the VLAN of a VM
def changeVLAN(content='',vm_uuid='',vlan_name='',is_VDS='',connected=True,startconnected=True):
    for device in vm_uuid.config.hardware.device:
        device_change = []
        if isinstance(device, vim.vm.device.VirtualEthernetCard):
            nicspec = vim.vm.device.VirtualDeviceSpec()
            nicspec.operation = vim.vm.device.VirtualDeviceSpec.Operation.edit
            nicspec.device = device
            nicspec.device.wakeOnLanEnabled = True
            if not is_VDS:
                nicspec.device.backing = vim.vm.device.VirtualEthernetCard.NetworkBackingInfo()
                nicspec.device.backing.network = get_obj(content, [vim.Network], vlan_name)
                nicspec.device.backing.deviceName = vlan_name
            else:
                network = get_obj(content, [vim.dvs.DistributedVirtualPortgroup], vlan_name)
                dvs_port_connection = vim.dvs.PortConnection()
                dvs_port_connection.portgroupKey = network.key
                dvs_port_connection.switchUuid = network.config.distributedVirtualSwitch.uuid
                nicspec.device.backing = vim.vm.device.VirtualEthernetCard.DistributedVirtualPortBackingInfo()
                nicspec.device.backing.port = dvs_port_connection
            nicspec.device.connectable = vim.vm.device.VirtualDevice.ConnectInfo()
            nicspec.device.connectable.startConnected = startconnected
            # The connected option only works if the VM is on. If there is an error, try removing this next line.
            nicspec.device.connectable.connected = connected
            nicspec.device.connectable.allowGuestControl = True
            device_change.append(nicspec)
            config_spec = vim.vm.ConfigSpec(deviceChange=device_change)
            vm_uuid.ReconfigVM_Task(config_spec)

# Function to modify the destination IP:PORT to send serial data to. Only works if target VM already
# has a virtual serial port. Be mindful of the ESXi hosts firewall when you choose a port to call out
# with as it may be blocked!
def changeSerial(vm_uuid='',dest_ip=''):
    device_change = []
    for device in vm_uuid.config.hardware.device:
        if isinstance(device, vim.vm.device.VirtualSerialPort):
            serialspec = vim.vm.device.VirtualDeviceSpec()
            serialspec.operation = vim.vm.device.VirtualDeviceSpec.Operation.edit
            serialspec.device = device
            serialspec.device.backing = vim.vm.device.VirtualSerialPort.URIBackingInfo()
            serialspec.device.backing.serviceURI = dest_ip  # This should be in the form of 'IP:PORT'
            serialspec.device.backing.direction = 'client'
            serialspec.device.connectable = vim.vm.device.VirtualDevice.ConnectInfo()
            serialspec.device.connectable.startConnected = True
            # The connected option only works if the VM is on. If there is an error, try removing this next line.
            serialspec.device.connectable.connected = True
            device_change.append(serialspec)
            break
    config_spec = vim.vm.ConfigSpec(deviceChange=device_change)
    vm_uuid.ReconfigVM_Task(config_spec)


# Function to create a clone from a template. More options can be added, but it was a pain to
# make sysprep work as well as it does, so modify at your own risk!
def clone(service_instance=None, template_vm=None, name=None, destfolder=None, productKey=None, os_type=None, cluster=None):
    content = service_instance.RetrieveContent()
    # Need to find some items within vSphere initially so we know where to put the VM
    print " - Locating resource pool"
    resource_pool = cluster.resourcePool
    print " - Locating datastore"
    datastore = findDatastore(service_instance)

    print " - Deploying VM %s" % name
    # Relocation spec
    relospec = vim.vm.RelocateSpec(datastore=datastore,pool=resource_pool)

    # The below sections are ONLY used if you want to use sysprep
    # Most of the code was taken from: https://github.com/vmware/thinapp_factory/blob/master/workpool/afdeploy/worker/linked.py
    # Set some universal variables
    customSpec = pyVmomi.vim.vm.customization.Specification()
    sysPrep = vim.vm.customization.Sysprep()
    guiUnattended = pyVmomi.vim.vm.customization.GuiUnattended()
    userData = pyVmomi.vim.vm.customization.UserData()
    globalIp = pyVmomi.vim.vm.customization.GlobalIPSettings()
    adapterMap = pyVmomi.vim.vm.customization.AdapterMapping()
    licenseFilePrintData = pyVmomi.vim.vm.customization.LicenseFilePrintData()
    GUIRunOnce = pyVmomi.vim.vm.customization.GuiRunOnce()

    # Setup network
    adapterMap.adapter = pyVmomi.vim.vm.customization.IPSettings()
    adapterMap.adapter.ip = pyVmomi.vim.vm.customization.DhcpIpGenerator()

    # Turn off auto-logon for first reboot
    guiUnattended.autoLogon = False
    # Reference: https://msdn.microsoft.com/en-us/library/ms912391%28v=winembedded.11%29.aspx
    guiUnattended.timeZone = 35
    sysPrep.guiUnattended = guiUnattended

    # Run commands on target after built, currently only runs if this is a plus out box.
    if (name[-1:] == '+') or (name[-2:] == '-p'):
        cmdList = []
        cmdList.append(r'cscript c:\Windows\System32\slmgr.vbs /ato')
        cmdList.append(r'reg add hklm\software\policies\microsoft\windows\windowsupdate /t REG_SZ /v WUServer /d http://wsus-kms:8530 /f')
        cmdList.append(r'reg add hklm\software\policies\microsoft\windows\windowsupdate /t REG_SZ /v WUStatusServer /d http://wsus-kms:8530 /f')
        GUIRunOnce.commandList = cmdList
        sysPrep.guiRunOnce = GUIRunOnce

    # Supply user specification data for sysprep
    userData.fullName='name'
    userData.orgName='org'
    userData.computerName = pyVmomi.vim.vm.customization.PrefixNameGenerator()
    # Need to filter out specific characters so that they are not put into computer name or widows will barf
    chars_to_remove = ['.', '!', '?', '_']
    userData.computerName.base = name.translate(None, ''.join(chars_to_remove))
    if productKey!=None:
        userData.productId = productKey
    sysPrep.userData = userData

    sysPrep.identification = pyVmomi.vim.vm.customization.Identification()
    sysPrep.identification.joinWorkgroup='WORKGROUP'

    # Next we need to specify license data for 2k and 2k3. But doesn't hurt if everything gets this setting.
    licenseFilePrintData.autoUsers=5
    licenseFilePrintData.autoMode='perServer'
    sysPrep.licenseFilePrintData = licenseFilePrintData

    # Putting all these pieces together in a custom spec
    customSpec.identity = sysPrep
    customSpec.globalIPSettings = globalIp
    customSpec.options = pyVmomi.vim.vm.customization.WinOptions()
    customSpec.options.changeSID = True
    customSpec.options.deleteAccounts = False
    customSpec.nicSettingMap = [adapterMap]

    # If the template OS is windows, we will run sysprep, otherwise do not.
    # NOTE: As of vCenter 6.7, XP and 2k3 are no longer supported for sysprep via vcenter
    if ("winXP" in os_type) or ("winNet" in os_type): # exempt XP and Server 2003
        clonespec = vim.vm.CloneSpec(location=relospec,customization=None,powerOn=True,template=False)     # Clone w/o sysprep
    elif ("win" in os_type):
        clonespec = vim.vm.CloneSpec(location=relospec,customization=customSpec,powerOn=True,template=False)    # Clone with sysprep
    else:
        clonespec = vim.vm.CloneSpec(location=relospec,customization=None,powerOn=True,template=False)     # Clone w/o sysprep

    # fire the clone task
    task = template_vm.Clone(folder=destfolder, name=name, spec=clonespec)
    result = WaitTask(task, 'VM clone task')

# This does require an existing known snapshot on the box.
def create_link_clone(service_instance,content,vm_uuid, clone_name, snapshot_name, dest_folder_uuid):
    # The magic piece to create a linked clone is the disMoveType option here :)
    relospec = vim.vm.RelocateSpec(diskMoveType="createNewChildDiskBacking")
    # Need to find the right snapshot to make the clone from.
    try:
        snapshots = vm_uuid.snapshot.rootSnapshotList[0].childSnapshotList
        snap_obj = vm_uuid.snapshot.rootSnapshotList[0].snapshot # set a variable just as a place holder
    except:
        return "No snapshots found"
    for snapshot in snapshots:
        if snapshot_name == snapshot.name:
            snap_obj = snapshot.snapshot
            break
        if (snapshot.childSnapshotList):
            for snapshot in snapshot.childSnapshotList:
                if snapshot_name == snapshot.name:
                    snap_obj = snapshot.snapshot
                    break
                if (snapshot.childSnapshotList):
                    for snapshot in snapshot.childSnapshotList:
                        if snapshot_name == snapshot.name:
                            snap_obj = snapshot.snapshot
                            break
                        if (snapshot.childSnapshotList):
                            for snapshot in snapshot.childSnapshotList:
                                if snapshot_name == snapshot.name:
                                    snap_obj = snapshot.snapshot
                                    break
                                if (snapshot.childSnapshotList):
                                    return 'Snapshot not found, possibly too many snapshots on VM'

    clonespec = vim.vm.CloneSpec(location=relospec,customization=None,powerOn=True,template=False,snapshot=snap_obj)
    task = vm_uuid.Clone(folder=dest_folder_uuid, name=clone_name, spec=clonespec)
    result = WaitTask(task, 'VM clone task')

# Creates a snapshot on a VM
def create_snapshot(vm_uuid, snapshot_name, snapshot_description):
    # Read about dumpMemory : http://pubs.vmware.com/vi3/sdk/ReferenceGuide/vim.VirtualMachine.html#createSnapshot
    dumpMemory = False
    quiesce = True
    WaitTask(vm_uuid.CreateSnapshot(snapshot_name, snapshot_description, dumpMemory, quiesce), 'Create Snapshot')

# Will revert then delete a snapshot on a VM
def delete_snapshot(vm_obj,snapshot_name):
    """
    This function will loop through 4 levels of snapshots
    """
    # Check the default snapshot before we start going through and checking the others.
    if vm_obj.snapshot.rootSnapshotList[0].name == snapshot_name:
        snap_obj = vm_obj.snapshot.rootSnapshotList[0].snapshot
        snap_obj.RevertToSnapshot_Task()
        snap_obj.RemoveSnapshot_Task(True)

    if (vm_obj.snapshot.rootSnapshotList[0].childSnapshotList):
        for snapshot in vm_obj.snapshot.rootSnapshotList[0].childSnapshotList:
            if snapshot_name == snapshot.name:
                snap_obj = snapshot.snapshot
                snap_obj.RevertToSnapshot_Task()
                snap_obj.RemoveSnapshot_Task(True)
                break
            if (snapshot.childSnapshotList):
                for snapshot in snapshot.childSnapshotList:
                    if snapshot_name == snapshot.name:
                        snap_obj = snapshot.snapshot
                        snap_obj.RevertToSnapshot_Task()
                        snap_obj.RemoveSnapshot_Task(True)
                        break
                    if (snapshot.childSnapshotList):
                        for snapshot in snapshot.childSnapshotList:
                            if snapshot_name == snapshot.name:
                                snap_obj = snapshot.snapshot
                                snap_obj.RevertToSnapshot_Task()
                                snap_obj.RemoveSnapshot_Task(True)
                                break
                            if (snapshot.childSnapshotList):
                                for snapshot in snapshot.childSnapshotList:
                                    if snapshot_name == snapshot.name:
                                        snap_obj = snapshot.snapshot
                                        snap_obj.RevertToSnapshot_Task()
                                        snap_obj.RemoveSnapshot_Task(True)
                                        break
                                    if (snapshot.childSnapshotList):
                                         print('Snapshot not found, possibly too many snapshots on VM')

# Sets the comments for a VM
def set_comment(vm_uuid, vm_comment):
    spec = vim.vm.ConfigSpec()
    spec.annotation = vm_comment
    WaitTask(vm_uuid.ReconfigVM_Task(spec), 'Add comment')

# Mark a VM as a template. WARNING: If its a template, you can't see it in VMware Workstation
def mark_vm_as_template(vm_uuid):
    vm_uuid.MarkAsTemplate()

# Mark a template as a VM. Usefull if you want to update a template then mark it back as a template
def mark_template_as_vm(content,vm_uuid):
    cluster = get_obj(content, [vim.ClusterComputeResource], 'Cluster') # replace Cluster with the actual cluster name of your env
    resource_pool = cluster.resourcePool
    vm_uuid.MarkAsVirtualMachine(resource_pool)

# Make sure you include the finale / or \ in your file path. Windows has to be \\ for file paths.
def copy2VM(service_instance, sourcePath, filename, destPath, vm_obj_id, creds):
    content = service_instance.RetrieveContent()
    # Try to upload a file to the target
    with open(sourcePath + filename, 'r') as myfile:
        file_contents = myfile.read()
    try:
        file_attribute = vim.vm.guest.FileManager.FileAttributes()
        url = content.guestOperationsManager.fileManager.InitiateFileTransferToGuest(vm_obj_id, creds, destPath + filename, file_attribute, len(file_contents), True)
        resp = requests.put(url, data=file_contents, verify=False)
        if not resp.status_code == 200:
            print(printColored("Error while uploading file. Does it already exist or permission denied.", "red"))
    except IOError, e:
        print e

def executeOnVM(service_instance,process2start,processArguments,vm_obj_id,creds):
    content = service_instance.RetrieveContent()
    try:
        pm = content.guestOperationsManager.processManager
        ps = vim.vm.guest.ProcessManager.ProgramSpec(programPath=process2start,arguments=processArguments)
        res = pm.StartProgramInGuest(vm_obj_id, creds, ps)
        return res
    except:
        print(printColored('Error: Could not execute command on target.', 'red'))

# This function is designed to mass move a LOT of VMs or templates from one datastore to another.
def move_vms(service_instance, srcDatastore=None, destDatastore=None):
    # First we need to find the source datastore
    container = service_instance.content.rootFolder
    datastores = service_instance.content.viewManager.CreateContainerView(container=container, type=[vim.Datastore], recursive=True)
    dsUUID = ""
    for d in datastores.view:
        if d.name == srcDatastore:
            dsUUID = d
    if not dsUUID:
        return "Error, source datastore not found."
    vm_properties = ["name","config.template","runtime.powerState","datastore"]
    # Grab some initial data from vcenter
    vm_data = ''
    root_folder = service_instance.content.rootFolder
    view = get_container_view(service_instance, obj_type=[vim.VirtualMachine])
    vm_data = collect_properties(service_instance, view_ref=view, obj_type=vim.VirtualMachine, path_set=vm_properties, include_mors=True)
    # Get rid of the extra ansii characters from the dictionary
    vm_data = convert(vm_data)
    for vm in sorted(vm_data,key=itemgetter("name")):
        if (dsUUID in vm["datastore"]):
            print "Migrating %s" % (vm["name"])
            if vm["runtime.powerState"] == 'poweredOn':
                print " - Shutting down"
                # We will attempt to cleanly shutdown the VM. if that fails, hard power it off
                try:
                    vm["obj"].ShutdownGuest()
                except:
                    vm["obj"].PowerOff()
            elif vm["runtime.powerState"] == 'suspended':
                print " - Powering on, then shutting down"
                vm["obj"].PowerOn()
                time.sleep(30)
                try:
                    vm["obj"].ShutdownGuest()
                except:
                    vm["obj"].PowerOff()
            elif vm["config.template"] == True:
                print " - Marking template as VM for migration"
                mark_template_as_vm(service_instance.RetrieveContent(),vm["obj"])
            # specify the new location of the VM, default is whichever datastore has the most free space
            # Ref: https://github.com/vmware/pyvmomi/blob/master/docs/vim/vm/RelocateSpec.rst
            cluster = get_obj(service_instance.RetrieveContent(), [vim.ClusterComputeResource], 'Cluster') # Replace Cluster with the actual cluster for your env
            if destDatastore:
                print " - Moving to datastore: %s" % (destDatastore)
                dest=get_obj(content,[vim.Datastore],destDatastore)
                if not dest:
                    return "Error, destination datastore not found."
                relocate_spec = vim.vm.RelocateSpec(datastore=dest,pool=cluster.resourcePool)
            else:
                print " - Finding datastore with the most free space"
                relocate_spec = vim.vm.RelocateSpec(datastore=findDatastore(service_instance),pool=cluster.resourcePool)
            # does the actual migration to host
            print " - Migrating VM"
            WaitTask(vm["obj"].Relocate(relocate_spec),"Migrating VM")
            print " - Returning the VM to its origional state"
            if vm["runtime.powerState"] == 'poweredOn' or vm["runtime.powerState"] == 'suspended':
                vm["obj"].PowerOn()
            elif vm["config.template"] == True:
                # Convert the VM back to a template
                mark_vm_as_template(vm["obj"])

