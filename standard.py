def install_and_import(package, library=None):
  '''Try to import a library, if it fails attempt to install it through
  pip. If pip fails bail. You must assign the function to a variable ( e.g.
  paramiko = install_and_import('paramiko') )

  :package: the name of the pypi package
  :library: [optional] the name of the module if different from package

  :return library: Returns the imported library
  '''
  import importlib
  if library is None:
      library = package
  try:
      return importlib.import_module(library)
  except ImportError:
      try:
          import pip
          pip.main(['install', package])
      except:
          print "Pip was unable to install {}, exitting.".format(package)
          sys.exit(1)
      finally:
          return importlib.import_module(library)

