# python libraries
If you would like to have these included as available for python, put them inside your /usr/local/lib/python folder and then you should be able to simply import them as is. These were tested and verified working with python 2.x and may not work for python 3.
